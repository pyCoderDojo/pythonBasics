# mit dem Operator '=' kann man einer Variablen einen Wert zuweisen
a = 2
# mit der Funktion 'print' kann man Informationen auf dem Bildschirm ausgeben
print(a)

print("---------------------")

# auf diese Weise kann man jederzeit weitere Varibalen erstellen
b = 3
print(b)

print("---------------------")

# Mit Variablen kann man auch rechnen
c = a + b
print(c)

print("---------------------")

# genau so kann man auch Texte einer Variablen zuweisen
text1 = "Hallo"
print(text1)
text2 = "Welt!"
print(text2)

print("---------------------")

# und auch der + Operator ist mit Text-Variablen zusammen verwendbar
text3 = text1 + " " + text2
print(text3)

print("---------------------")

# mehr Details zum dem Thema findest du hier:
# https://www.python-lernen.de/variablen-einsetzen.htm
