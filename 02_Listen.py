# Listen sind Variablen die viele Werte enthalten können
# eine leere Liste erzeugt man so
a = []
# Listen kann man auch wie gewohnt ausgeben
print(a)

print("---------------------")

# eine Liste mit Werten legt man so an
b = [1,2,3,4,5]
print(b)

print("---------------------")

# Listen können auch Werte unterschiedlichen Typs enthalten
c= [1,2, "Hallo Welt!", 3.2, "a"]
print(c)

print("---------------------")

# Listen können auch jederzeit um weitere Werte erweitert werden
d = []
print(d)
d.append(1)
print(d)
d.append(2)
print(d)

print("---------------------")

# auf die Werte einer Liste kann man auch direkt zugreifen
# dazu verwendet man einen Index, der bei 0 beginnt
# das erste Element einer Liste hat also den Index 0, das 2. Element den Index 1, ...
e = [1,2,3,4,5,6,7,8,9,10]
e = [1,2,3,4,5,6,7,8,9,10]
print(e[0]) # -> 1
print(e[4]) # -> 5
print(e[9]) # -> 10

print("---------------------")

# man kann Listen auf teilen
# so erhält man die ersten 5 Werte der Liste
print(e[:5]) # -> [1, 2, 3, 4, 5]
# so erhält man die letzten 5 Werte der Liste
print(e[5:]) # -> [6, 7, 8, 9, 10]
# so erhält man jedes 2. Element der Liste
print(e[::2]) # -> [1, 3, 5, 7, 9]
# und so kann man die Liste umkehren
print(e[::-1]) # -> [10, 9, 8, 7, 6, 5, 4, 3, 2, 1]
# e.reverse()
# print(e) # -> [10, 9, 8, 7, 6, 5, 4, 3, 2, 1]

# ab dem 3. Element bis zum 5. Element
print(e[3:5]) # -> [4,5]
print("---------------------")

# und so kann man sich aus einer Liste eine neue Liste erstellen, die Variable f ist wieder ein Liste
f = e[::2] # -> [1, 3, 5, 7, 9]
print(f)

print("---------------------")

a = [1,2,3,4,5,6,7,8]
# iterieren über die Elemente einer Liste
for b in a:
    print(b)

print("---------------------")

a = [1,2,3,4,5,6,7,8]
# prüfen ob eine Element in einer Liste enthalten ist]
if 5 in a:
    print("ja, enthalten")
else:
    print("nein, nicht enthalten")

print("---------------------")

# mehr Details zum dem Thema findest du hier:
# https://www.python-lernen.de/listen.htm
