# ein wichtige Hilfe ist die Funktion range(a, b, c), die eine Liste von Ganzzahlen erzeugt
# a = Startwert
# b = obere Schranke, nicht enthalten
# c = schrittweite, default = 1, optional

print("---------------------")

a = list(range(1,10))
print(a) # -> [1, 2, 3, 4, 5, 6, 7, 8, 9]

print("---------------------")

b = list(range(1,10, 2))
print(b) # -> [1, 3, 5, 7, 9]

print("---------------------")

# for Schleife
for i in range(1,10):
    print(i) # -> 1..9

# eine for Schleife kann unterbrochen werden
for i in range(1,10):
    print(i) # -> 1..5
    if i == 5:
        break

print("---------------------")

d = ['a', 'b', 'c']
for j in d:
    print(j) # -> a, b, c

print("---------------------")

# while Schleife
e = True
f = 0
while e:
    f += 1
    if f == 5:
        e = False
    print(f) # -> 1...5

print("---------------------")

g = 0
while g<5:
    g += 1
    print(g) # -> 1...5

print("---------------------")

# mehr Details zum dem Thema findest du hier:
# https://www.python-lernen.de/while-schleife.htm
# https://www.python-lernen.de/for-schleife.htm
# https://www.python-lernen.de/break-continue-schleifen-ablauf.htm
