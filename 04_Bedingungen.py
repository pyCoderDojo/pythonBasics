# einfache Bedingung

print("---------------------")

a = 1
if a == 1:
    print(a)

print("---------------------")

b = True
if b:
    print(b)
else:
    print(b)

print("---------------------")

wochentage = ['Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa', 'So']

tag01 = wochentage[3]
if(tag01 == wochentage[0]):
    print('Montag')
elif (tag01 == wochentage[1]):
    print('Dienstg')
elif (tag01 == wochentage[2]):
    print('Mittwoch')
elif (tag01 == wochentage[3]):
    print('Donnerstag')
elif (tag01 == wochentage[4]):
    print('Freitag')
elif (tag01 == wochentage[5]):
    print('Samstag')
else:
    print('Sonntag')
# -> Donnerstag

# mehr Details zum dem Thema findest du hier:
# https://www.python-lernen.de/if-abfrage-python.htm
