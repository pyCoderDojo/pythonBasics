
print("---------------------")

a = {}
a['key1'] = 123
a['key2'] = 321
a['key3'] = "Hallo Welt!"

print(a) # -> {'key1': 123, 'key2': 321}
print(a['key1']) # -> 123
print(a['key2']) # -> 321

print("---------------------")

if 'key3' in a:
    print("key3 is a valid key")
if not 'key4' in a:
    print("key4 is not a valid key")

print("---------------------")

for i in a:
    print(i) # -> prints keys
    print(a[i]) # -> prints values assign to keys

print("---------------------")

# mehr Details zum dem Thema findest du hier:
# https://www.python-lernen.de/python-dictionary.htm

