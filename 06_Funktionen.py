print("---------------------")

# eine einfache Funktion, ohne Parameter und ohne Rückgabewert
def eineFunktion1():
    print("Hallo Welt!") # -> Hallo Welt!

eineFunktion1()

print("---------------------")

# eine Funktion, ohne Parameter, aber mit einem Rückgabewert
def eineFunktion2():
    return "Hallo Welt!"

print(eineFunktion2()) # -> Hallo Welt!

print("---------------------")

# eine Funktion, mit einem Parameter und mit einem Rückgabewert
def eineFunktion3(parameter):
    return parameter

print(eineFunktion3("Hallo Welt!")) # -> Hallo Welt!

print("---------------------")

# eine Funktion, mit zwei Parametern und einem berechneten Rüçkgabewert
def eineFunktion4(parameter1, parameter2):
    return parameter1 + parameter2

print(eineFunktion4(1,2)) # -> 3

print("---------------------")

# eine Funktion kann auch als Wert einer Variablen zugewiesen werden
f = eineFunktion4
# hier wird nun effektiv die weiter oben definierte 'eineFunktion4' aufgerufen

print(f) # -> <function eineFunktion4 at 0x7fa4a0096700>
print(f(5,3)) # -> 8
print(eineFunktion4(5,3)) # -> 8

print("---------------------")

# mehr Details zum dem Thema findest du hier:
# https://www.python-lernen.de/funktionen-in-python.htm
