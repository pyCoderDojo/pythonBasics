print("---------------------")

# einfache Verknüpfung von zwei Zeichenketten
z = "Keks" + "Wurst"
print(z)

print("---------------------")

a = 77
# einfache Verknüpfung von einer Zeichenkette mit eine Zahl
print(z + str(a))

b = 13/7
c = "Wurst"
d = "Keks"
# eleganter Weg eine Zeichenkette um Inhalte von Variablen unterschtiedlichem Typs zusammen zu bauen
x = f"%s %s %05d %5.2f" % (d, c, a, b)
print(x)

print("---------------------")

# mehr Details zum dem Thema findest du hier:
# https://www.python-lernen.de/operatoren-strings.htm