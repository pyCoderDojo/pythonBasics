import os

# hier wird der Umgang mit Dateien erläutert, welche Text Daten enthalten
# der Umgang mit bniären Daten unterscheidet sich jedoch nur an ein paar Stellen

print("---------------------")

# so öffnet man eine Datei im Modus 'lesend', d.h. es ist so nicht möglich den Inhalt zu verändern
file = open("README.md", 'r')
# so liest man alle Zeichen einer Datei bis zum ersten Zeilenumbruch
line = file.readline()
print(line)
# wird eine geöffnete Datei nicht mehr verwendet, dann muss sie geschlossen werden
file.close()

print("---------------------")

# so liest man alle Zeilen einer Datei, bis das Ende der Datei erreicht ist
# in der Variante des Zugriffs auf Dateien erfolgt das Lesen und spätere Schließen implizit
for line in open("README.md", 'r'):
    print(line)

print("---------------------")

file = open("test.txt", 'w')
file.write("Hallo Welt!")
file.close()

file = open("test.txt", 'r')
line = file.readline()
print(line)

print("---------------------")

# so kann auf das Vorhandensein einer Datei geprüft werden
filename = "test.txt"
if (os.path.isfile(filename)):

    # so kann eine Datei gelöscht werden
    os.remove(filename)

    print(f"das Löschen der Datei %s war erfolgreich" % filename)
else:
    print(f"die Datei %s existiert nicht" % filename)

print("---------------------")

# mehr Details zum dem Thema findest du hier:
# https://www.python-lernen.de/dateien-auslesen.htm
# https://www.python-lernen.de/in-dateien-schreiben.htm
# https://www.python-lernen.de/csv-datei-einlesen.htm