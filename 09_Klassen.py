class EineKlasse:

    def __init__(self, name, alter=0):
        self.vorname = name
        self.alter = alter

    def hallo(self):
        print(f"Hallo %s" % self.vorname)
        if self.alter > 0:
            print(f"alter %d" % self.alter)

        self.__puups()

    def __puups(self):
        print(1+1)

einklasse = EineKlasse(name="Hans")
nocheineklasse = EineKlasse(name="Hugo", alter=34)

einklasse.hallo()
nocheineklasse.hallo()
