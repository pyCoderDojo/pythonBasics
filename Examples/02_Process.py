
inFile = "/Users/dermicha/Downloads/Import_Cortezza.CSV"
data = open(inFile, "r")
dataOut = open(f"%s.new.csv" % (inFile), "w")
sep = ";"


def readStatusMapping(filename, sep=","):
    sm = {}
    for l in open(filename, "r"):
        if "status" not in l.lower():
            d = l.split(sep)
            sm[d[1].strip()] = d[0].strip()
    return  sm


statusMapping = readStatusMapping("/Users/dermicha/Downloads/oohbase - WerbeflaechenStatus - all-2.csv")

dataOut.write("\"Mandantkuerzel\",\"Status\",\"Werbeflaechennummer\",\"Format\",\"Objekt\",\"WFFotoEigen\"\n")
for line in data:
    if not "Mandantkuerzel".lower() in line.lower():
        line = line.replace("\n", "")
        d = line.split(sep)
        id = d[0].strip()
        try:
            status = statusMapping[d[1].strip().lower()]
        except:
            status = d[1].strip().lower()
        nummer = d[2].strip().upper()
        format = d[3].strip().upper().replace("''", " Zoll")
        objekt = d[4].strip()
        foto = d[5].strip()
        out = f"\"%s\",%s,\"%s\",\"%s\",\"%s\",\"%s\"" % (
            id, status, nummer, format, objekt, foto
        )
        dataOut.write(f"%s%s" % (out, "\n"))
