
# tree_height =6
# trunk_width = 3
#
# i=0| 12345*12345 (1)
# i=1| 1234***1234 (3)
# i=2| 123*****123 (5)
# i=3| 12*******12 (7)
# i=4| 1*********1 (9)
# i=5| *********** (11)
# i=0| 1234***1234 (3)
# i=1| 1234***1234 (3)

# tree_height =3
# trunk_width = 1
#
# i=0| 12*12 (1)
# i=1| 1***1 (3)
# i=2| ***** (5)
# i=0| 12*12 (1)

def draw_tree(height, width):
    tree_height=height
    trunk_height=int((tree_height/3)+0.5)
    trunk_width=width

    for i in range(0,tree_height):
        print((tree_height-i-1) * " ", ((2*i)+1) * "*")
    for i in range(0, trunk_height):
        print(int((((2*tree_height)-trunk_width)/2))*" ", trunk_width*"*")


for h in range(2, 10+1):
    for w in range(1,3+1,2):
        draw_tree(h,w)
        print(20*"-")
