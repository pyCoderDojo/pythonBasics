#!/bin/bash

CMD=$1

MYIMAGE="my-programm-img"

DOCKER=`which docker`

if [[ "build" == ${CMD} ]]; then

  IMAGE=(`${DOCKER} images | grep ${MYIMAGE} | sed -E -e 's/[ ]+/;/g'|tr ";" "\n"`)
  if [[ ! -z ${IMAGE} ]]; then
    ${DOCKER} rmi --force ${IMAGE[2]}
  fi

  # build
  docker build --tag ${MYIMAGE} .
fi

docker run --rm  ${MYIMAGE}
# test it