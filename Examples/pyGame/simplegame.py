import logging

import pygame
from pygame.constants import *

import sys
import Examples.pyMqtt.mqttdatabus as mqttbus
import uuid

# Initialize program
FORMAT = '%(asctime)-20s [%(levelname)s]: %(message)s'
logging.basicConfig(level=logging.INFO, format=FORMAT)

pygame.init()

localPlayerId = f"player-{uuid.uuid4().hex}"

# mqttServer = "localhost"
mqttServer = "mqtt.eclipseprojects.io"
mqttTopicBase = "coderdojo/mastermind"
mqb = mqttbus.MqttDataBusMessage(localPlayerId, mqttTopicBase, server=mqttServer)
mqb.startDataBus()

# Assign FPS a value
FPS = 30
FramePerSec = pygame.time.Clock()

# Setting up color objects
BLUE = (0, 0, 255)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)

# Setup a 300x300 pixel display with caption
DISPLAYSURF = pygame.display.set_mode((600, 600))
DISPLAYSURF.fill(BLUE)
pygame.display.set_caption("Simnple PyGame")

SCREEN_WIDTH, SCREEN_HEIGHT = pygame.display.get_window_size()


class Player(pygame.sprite.Sprite):

    def __init__(self, sprite, mqttBus):
        super().__init__()
        self.image = pygame.image.load(sprite)
        self.image = pygame.transform.scale(self.image, (200, 200))
        self.rect = self.image.get_rect()
        self.rect.center = (SCREEN_HEIGHT/2, SCREEN_WIDTH/2)
        self.mqttBus = mqttBus
        self._newBorn = True
        self._stepsize = 5

    def update(self):
        send = False
        pressed_keys = pygame.key.get_pressed()

        if self.rect.top > self._stepsize:
            if pressed_keys[K_UP]:
                self.rect.move_ip(0, -self._stepsize)
                send = True
        if self.rect.bottom < SCREEN_HEIGHT - self._stepsize:
            if pressed_keys[K_DOWN]:
                self.rect.move_ip(0, self._stepsize)
                send = True
        if self.rect.left > 5:
            if pressed_keys[K_LEFT]:
                self.rect.move_ip(-self._stepsize, 0)
                send = True
        if self.rect.right < SCREEN_WIDTH - self._stepsize:
            if pressed_keys[K_RIGHT]:
                self.rect.move_ip(self._stepsize, 0)
                send = True
        if send or self._newBorn:
            (x,y) = self.rect.center
            self.mqttBus.sendData(
                f"""{{
                    "x": {x},
                    "y": {y}
                }}"""
            )
            self._newBorn = False

    def draw(self, surface):
        surface.blit(self.image, self.rect)


class RemotePlayer(pygame.sprite.Sprite):

    def __init__(self, sprite, mqttBus):
        super().__init__()
        self.image = pygame.image.load(sprite)
        self.image = pygame.transform.scale(self.image, (200, 200))
        self.rect = self.image.get_rect()
        self.curPos = (SCREEN_HEIGHT/2, SCREEN_WIDTH/2)
        self.rect.center = self.curPos
        self._mqttBus = mqttBus

    def update(self):
        if self._mqttBus.hasData():
            data = self._mqttBus.getData().getJsonData()
            self.curPos = (data['x'], data['y'])

        self.rect.center = self.curPos

    def draw(self, surface):
        surface.blit(self.image, self.rect)


def addRemotePlayer(playerId, mqttTopicBase, mqttBus):
    if not playerId in remotePlayerIds:
        rp = RemotePlayer("./img/robot/Run (1).png", mqttBus)
        players.append(rp)
        remotePlayerIds.add(playerId)


P1 = Player("./img/dino/Run (1).png", mqb)
players = [P1]
remotePlayerIds = set()

while True:
    # Code

    for event in pygame.event.get():

        if event.type == QUIT:
            pygame.quit()
            sys.exit()

    for p in players:
        p.update()

    DISPLAYSURF.fill(WHITE)
    for p in players:
        p.draw(DISPLAYSURF)

    for rc in mqb.getRemoteClients():
        addRemotePlayer(rc, mqttTopicBase, mqb)

    # rps = len(mqb.getRemoteClients())
    # logging.info(f"total remote players: %d" % (rps))

    pygame.display.update()
    FramePerSec.tick(FPS)
