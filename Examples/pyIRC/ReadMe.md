# simple demo of IRC as an message broker

## usage 

shell 1:

    python irctester.py irc.libera.chat mybot1 "mybot2,mybot3"

shell 2:

    python irctester.py irc.libera.chat mybot2 "mybot1,mybot3"

shell 3:

    python irctester.py irc.libera.chat mybot3 "mybot1,mybot2"

