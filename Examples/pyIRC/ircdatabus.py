import irc.bot
import irc.client
import json
import threading
import logging


class IrcDataBusMessage():
    def __init__(self, senderId, data):
        self.senderId = senderId
        self.data = data


class IrcDataBus(irc.bot.SingleServerIRCBot):


    def __init__(self, ownId, targetIds, server, port=6667):
        """"
        Attributes:
            ownId: String
            targetIds: [String]
            server: String
            port: int
        """

        irc.bot.SingleServerIRCBot.__init__(self, [(server, port)], ownId, ownId)
        self._ownId = ownId
        self._targetIds = targetIds.split(",")
        try:
            self.connect(
                server=server,
                port=port,
                nickname=ownId
            )
        except Exception as e:
            logging.error("connection could not be established")

        self._messages = []


    def on_privmsg(self, c, e):
        semderId = irc.client.NickMask(e.source)
        msg = IrcDataBusMessage(
            senderId = semderId.nick,
            data = e.arguments[0]
        )
        self._messages.append(msg)

    def _doConnect(self):
        try:
            if not self.connection.is_connected():
                self.connection.reconnect()
            return True;
        except:
            logging.error("reconnect failed")
            return False;

    def _hasData(self):
        if len(self._messages) > 0:
            return True
        else:
            return False


    def getData(self):
        if self._hasData():
            msg = self._messages[-1]
            self._messages.remove(msg)
            return msg
        else:
            return None


    def _start(self):
        self.start()


    def startDataBus(self):
        self.runner = threading.Thread(target=self._start)
        self.runner.start()


    def sendJsonData(self, jsonData):
        jsonStr = json.dumps(jsonData)
        return self.sendData(jsonStr)


    def sendData(self, data):
        logging.debug(f"send >>%s<<" % data )
        if self._doConnect():
            for tid in self._targetIds:
                logging.debug(f"to %s" % tid)
                self.connection.privmsg(tid, data)
            return True
        else:
            self.connection.reconnect()
            ic("not connected, could not send data")
            return False
