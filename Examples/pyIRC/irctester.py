import sys
import time
import uuid
import ircdatabus
import atexit
import logging

from datetime import datetime


@atexit.register
def exit():
    print("exit requested")
    sys.exit(2)

FORMAT = '%(asctime)-20s [%(levelname)s]: %(message)s'
logging.basicConfig(level=logging.INFO, format=FORMAT)


if len(sys.argv) != 4:
    print("Usage: IrcDataBus <senderId> <targetIds> <server>")
    sys.exit(2)

server = sys.argv[1]
ownId = sys.argv[2]
targetIds = sys.argv[3]

# server = "irc.libera.chat"
# server = "open.ircnet.net"
# ownId = "mybot1"
# targetIds = "mybot1"


logging.info(f" starting as %s, connected with %s" % (ownId, targetIds))

logging.info("try to start IRC bot")
idb = ircdatabus.IrcDataBus(ownId, targetIds, server)
idb.startDataBus()
logging.info("started IRC bot")

while 1:
    if idb._hasData():
        msg = idb.getData()
        logging.info(f"%s send %s" % (msg.senderId, msg.data))
    else:
        time.sleep(2)

    data = {
        "timestamp": f"%s" % datetime.now(),
        "id": uuid.uuid4().hex
    }
    idb.sendJsonData(data)
