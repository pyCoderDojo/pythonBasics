import paho.mqtt.client as mqtt
import json
import logging


class MqttDataBusMessage():

    def getData(self):
        return self._data

    def getJsonData(self):
        try:
            return json.loads(self._data)
        except:
            return None

    def getSenderId(self):
        return self._senderId

    def getSenderTopic(self):
        return self._stopic

    def __init__(self, topic, sid, data):
        self._stopic = topic
        self._senderId = sid
        self._data = data


class IrcDataBus():

    def __initMqtt(self, stopic, server, port):
        def on_connect(client, userdata, flags, rc):
            logging.debug("Connected with result code " + str(rc))
            logging.debug("subscribe to: ", stopic)
            client.subscribe(stopic)

        # The callback for when a PUBLISH message is received from the server.
        def on_message(client, userdata, msg):
            stopic = msg.topic
            splitted = stopic.split("/")
            sid = splitted[splitted.index("data") - 1]
            dbmsg = MqttDataBusMessage(stopic, sid, msg.payload.decode('UTF-8'))

            if sid != self._ownId:
                self._dataBuffer.append(dbmsg)
                self._remoteClients.add(sid)
                if len(self._dataBuffer)>self._dataBufferMax:
                    self._dataBuffer = self._dataBuffer[1:]

            logging.debug(f"sender: {sid} / data: {dbmsg._data}")

        client = mqtt.Client()
        client.on_connect = on_connect
        client.on_message = on_message

        client.connect(server, port, 60)
        self.mqttClient = client

    def __init__(self, ownId, baseTopic = "coderdojo/examples", server="mqtt.eclipseprojects.io", port=1883, dataBufferMax = 100):
        """"
                Attributes:
                    ownId: String
                    server: String
                    port: int
                """

        self._ownTopic = f"{baseTopic}/{ownId}/data"
        self.__initMqtt(stopic=f"{baseTopic}/+/data", server=server, port=port)
        self._ownId = ownId
        self._dataBuffer = []
        self._dataBufferMax = dataBufferMax
        self._remoteClients = set()

    def getRemoteClients(self):
        return self._remoteClients.copy()

    def startDataBus(self):
        self.mqttClient.loop_start()

    def sendJsonData(self, jsonData):
        jsonStr = json.dumps(jsonData)
        return self.sendData(jsonStr)

    def sendData(self, data):
        self.mqttClient.publish(self._ownTopic, data, qos=1, retain=False)

    def bufferSize(self):
        return len(self._dataBuffer)

    def hasData(self):
        if self.bufferSize() > 0:
            return True
        return False

    def getData(self):
        if self.hasData():
            msg = self._dataBuffer[0]
            self._dataBuffer.remove(msg)
            return msg
        else:
            return None
