import json
import logging
import mqttdatabus
import uuid
import time
import random

# mqttServer = "mqtt.eclipseprojects.io"
mqttServer = "localhost"
mqttTopicBase = "coderdojo/pybasics"
dataBufferMax = 10

FORMAT = '%(asctime)-20s [%(levelname)s]: %(message)s'
logging.basicConfig(level=logging.INFO, format=FORMAT)

mqb1 = mqttdatabus.MqttDataBusMessage(uuid.uuid4().hex, mqttTopicBase, mqttServer, dataBufferMax=dataBufferMax)
mqb1.startDataBus()

mqb2 = mqttdatabus.MqttDataBusMessage(uuid.uuid4().hex, mqttTopicBase, mqttServer, dataBufferMax=dataBufferMax)
mqb2.startDataBus()

mqb3 = mqttdatabus.MqttDataBusMessage(uuid.uuid4().hex, mqttTopicBase, mqttServer, dataBufferMax=dataBufferMax)
mqb3.startDataBus()

random.seed()
while True:
    logging.info(f"current data buffer size client 1: {mqb1.bufferSize()}")
    logging.info(f"current data buffer size client 2: {mqb2.bufferSize()}")
    logging.info(f"current data buffer size client 3: {mqb3.bufferSize()}")
    logging.info(f"remote clients: {mqb1.getRemoteClients()}")


    if (mqb1.bufferSize() > 5):
        dbmsg = mqb1.getData()
        jdat = dbmsg.getJsonData()
        logging.info(f"x: {jdat['x']} / y: {jdat['y']}")

    data = f"{{\"x\": %d,\"y\":%d}}" % (random.randint(0, 1024), random.randint(0, 768))

    mqb2.sendData(data)
    time.sleep(1)
