# https://docs.python.org/3/library/sqlite3.html
# https://www.sqlite.org/docs.html

import sqlite3
import datetime
import loremipsum

# generates random sentences based on loremipsum library
def getSentences(n):
    sentences_list = loremipsum.get_sentences(n)
    sentences = " ".join(sentences_list).replace('B\'', '').replace('\'', '')
    return sentences


# define a filename that will be used by SQLLite3
mydbFilename = 'mydatabase.db'
# now we connect to our database, which means open the file or create it
mydbConnection = sqlite3.connect(mydbFilename)

# create a session to talk to SQLLite
session = mydbConnection.cursor()

# cleanup our database, make sure that the following create table will work
session.execute('''
    DROP TABLE IF EXISTS my_data
    ''')

# create an empty database table
session.execute('''
    CREATE TABLE my_data
               (id INTEGER PRIMARY KEY autoincrement, data text, timestamp text UNIQUE)
    ''')

# now insert some random data into our new table
for i in range(100):
    now = datetime.datetime.now().isoformat()
    data = getSentences(3)

    session.execute("INSERT INTO my_data (data, timestamp) VALUES (?, ?)", (data, now))
    createdRowId =session.lastrowid
    print(createdRowId, end=', ')

print()

# now we persist our changes
mydbConnection.commit()

# read data we have just inserted into our table
for row in session.execute('SELECT * FROM my_data ORDER BY timestamp desc limit 10'):
    row_id = row[0]
    data = row[1]
    timestamp = row[2]
    print(f"id: %d, data: %s, timestamp: %s" % (row_id, data, timestamp))


# delete all rows
print("delete all data")
session.execute("DELETE FROM my_data")
mydbConnection.commit()

# check if table is empty
for row in  session.execute('SELECT count(*) FROM my_data'):
    print(f"number of rows: %d" % row[0])

# finally close our current session
mydbConnection.close()
