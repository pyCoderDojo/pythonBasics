import logging
import time
import pyScraperFileLib as myflib
import pyScraperLib as mylib
import requests

# init logging
FORMAT = '%(asctime)-20s [%(levelname)s]: %(message)s'
logging.basicConfig(level=logging.INFO, format=FORMAT)
# logging.basicConfig(level=logging.DEBUG, format=FORMAT)

wordFile = "words.csv"

# baseUrl = 'https://api.duckduckgo.com?format=json&q='
# baseUrl = 'https://www.thefreedictionary.com/'
# baseUrl = 'https://api.datamuse.com/words?md=fdPrs&ml='

# baseUrl = 'https://api.datamuse.com/words?md=fpd&ml='
# baseUrl = 'https://api.datamuse.com/words?md=d&sl=' #using spells like to get a better result
# wordDumpPath = "wordsDatamuse"
# headers = {}

# baseUrl = 'https://api.dictionaryapi.dev/api/v2/entries/en/'
#wordDumpPath = "wordsDictionaryApi"
# headers = {}

baseUrl = "https://od-api.oxforddictionaries.com:443/api/v2/entries/" + "en-gb" + "/" 
wordDumpPath = "oxfordDictionariesAPI"
headers = {"app_id": "cbd23098", "app_key": "13009b7a99bb39c0b94a8509cb2401dc"}

def getEtag(headers):
    if "etag" in headers.keys():
        return headers['etag']
    else:
        return None

def downloadWord(word, wordDataPath, baseUrl):
    logging.info(f"download word: %s" % (word))
    url = f"%s%s" % (baseUrl, word)
    wordDatFile = f"%s/%s.json" % (wordDataPath, word)
    wordDatEtag = f"%s/%s.etag" % (wordDataPath, word)
    if myflib.isEmptyFile(wordDatFile) or not myflib.isEmptyFile(wordDatEtag):
        etag=myflib.readFile(wordDatEtag)
        (response, error) = mylib.download(url, etag,headers, False)
        if error == None:
            doc = response.text
            etag = getEtag(response.headers)
            if myflib.storeDoc(doc, wordDatFile) and myflib.storeDoc(etag, wordDatEtag):
                return True
            else:
                logging.error(f"failed to store (%s) data for word: %s" % (wordDatFile, word))
                return False
        else:
            logging.warning(f"not downloaded (%s) data for word: %s (%s)" % (url, word, error))
            return False
    else:
        logging.warning(f"word already downloaded: %s" % (word))
        return False


if myflib.existsFile(wordFile):
    if myflib.createDataPath(wordDumpPath):
        for w in open(wordFile, 'r'):
            w = w.strip().lower()
            if w > '':
                if downloadWord(w, wordDumpPath, baseUrl):
                    time.sleep(2)


exit(0)
