import os
import logging


def storeDoc(data, datafile, overwrite=True):

    try:
        if(data != None):
            f = open(datafile, 'w')
            f.write(data)
            f.close()
            return True
        else:
            return False
    except Exception as e:
        logging.error(f"could not store data to file: %s" % (datafile))
        return False


def createDataPath(path):
    if os.path.exists(path):
        if os.path.isdir(path):
            return True
        else:
            logging.debug(f"not a directory: %s" % (path))
    else:
        try:
            os.mkdir(path)
            return True
        except Exception as e:
            logging.error(f"failed to create directory: %s" % (path))
            return False


def existsFile(filepath):
    if os.path.exists(filepath):
        if os.path.isfile(filepath):
            return True
        else:
            logging.debug(f"not a file: %s" % (filepath))
            return False
    else:
        logging.debug(f"file does not exist: %s" % (filepath))
        return False


def isEmptyFile(filename):
    if existsFile(filename) and os.path.getsize(filename) > 0:
        return False
    else:
        return True

def readFile(filename):
    if os.path.exists(filename) and os.path.isfile(filename):
        tempData=""
        for l in open(filename, 'r'):
            tempData += l
        return tempData
    else:
        return None
