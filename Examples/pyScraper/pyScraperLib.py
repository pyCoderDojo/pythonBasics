import logging

import requests

def download(url, etag, headers, json=True):
    logging.info(f"download: %s" % url)
    #headers = {}
    if json:
        headers["accept"] = "application/json; charset=utf-8"
    if etag != None:
        headers["If-None-Match"] = etag

    logging.debug(f"used headers: %s" % (headers))

    try:
        response = requests.get(url, headers=headers)

        if response.status_code < 300:
            return (response, None)
        elif response.status_code == 304:
            errorMsg = f"Status %d: %s" % (response.status_code, "content not changed")
            return (None, errorMsg)
        else:
            errorMsg = f"Status %d: %s" % (response.status_code, response.text)
            return (None, errorMsg)
    except Exception as e:
        return (None, f"Connection error: %s" % (e))