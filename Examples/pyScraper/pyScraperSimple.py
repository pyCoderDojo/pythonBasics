import time
import pyScraperLib as mylib

words = ['boat', 'house', 'plane', 'bike', 'run', 'swim']
# words = ['boat']
# words = ['abalone']


# baseUrl = 'https://api.dictionaryapi.dev/api/v2/entries/en/'
# baseUrl = 'https://api.duckduckgo.com?format=json&q='
# baseUrl = 'https://huhu.de/q='
# baseUrl = 'https://www.thefreedictionary.com/'
# baseUrl = 'https://api.datamuse.com/words?md=fdPrs&ml='
baseUrl = 'https://api.datamuse.com/words?md=fpd&ml='


class SearchResonse:

    def __init__(self, searchString, content=None, contentJson=None, error=None):
        self.searchString = searchString
        self.content = content
        self.contentJson = contentJson
        self.error = error


def throttledDownload(urls, json=True, delay=2):
    print(f"throttled download: %s" % urls)
    responses = []

    for k in urls.keys():
        u = urls[k]
        (doc, error) = mylib.download(u, json)
        if error == None:
            if json:
                sr = SearchResonse(searchString=k, contentJson=doc)
            else:
                sr = SearchResonse(searchString=k, content=doc)
        else:
            sr = SearchResonse(searchString=k, error=error)
        responses.append(sr)

        if u != list(urls.values())[-1]:
            time.sleep(delay)

    return responses


def downloadWords(baseUrl, words):
    print(f"download words: %s" % (words))
    urls = {}
    for w in words:
        u = f"%s%s" % (baseUrl, w)
        urls[w] = u

    return throttledDownload(urls, json=True, delay=1 / 10)


wordDB = {}
for i in range(0,5):
    srs = downloadWords(baseUrl, words)
    for sr in srs:
        if sr.error == None:
            for d in sr.contentJson:
                w = d['word']
                wordDB[w] = d
        else:
            print(sr.error)
    words = wordDB.keys()

print(len(wordDB.keys()))
