import os
import json

basePath_DictionaryAPI = "wordsDictionaryApi/"
basePath_Datamuse = "wordsDatamuse/"
basePath_Oxford = "oxfordDictionariesAPI/"

def export(getWord, getDefinition,basePath,fileName):
    csv_file = open(fileName,"w")
    wordFile = "words.csv"
    

    for w in open(wordFile, 'r'):
        #print(w.strip())
        filename = basePath + w.strip() + ".json"
        if(os.path.exists(filename)):
            #print("Found")
            with open(filename, 'r') as file:
                data = json.load(file)

                #word = get_word_DictionaryAPI(data)
                #definition = get_definition_DectionaryAPI(data)

                word = getWord(data)
                definition = getDefinition(data)
                
                #print in export file
                csv_file.write(word + ";" + "\"" + definition + "\"" + "\n")     
    csv_file.close()



# functions for DictionaryAPI
def get_word_DictionaryAPI(data):
    return data[0]['word']

def get_definition_DictionaryAPI(data):
    return data[0]['meanings'][0]['definitions'][0]['definition'].replace("\"","'")

# functions for Datamuse
def get_word_Datamuse(data):
    return data[0]['word']

def get_definition_Datamuse(data): 
    definition = ""
    for d in data[0]["defs"]:
        definition = definition + d.replace("\"","'").replace("\t","").replace("n","",1) + "\n"

    return definition.strip()
    # return data[0]["defs"][0].replace("\"","'").replace("\t","").replace("n","",1)

# functions for Oxford
def get_word_Oxford(data):
    return data["word"]

def get_definition_Oxford(data):
    #return "def"
    return data["results"][0]["lexicalEntries"][0]["entries"][0]["senses"][0]["definitions"][0].replace("\"","'")



# Aufruf
# export(get_word_DictionaryAPI,get_definition_DictionaryAPI,basePath_DictionaryAPI,"export_da.csv")
# export(get_word_Datamuse,get_definition_Datamuse,basePath_Datamuse,"export_dm.csv")
export(get_word_Oxford,get_definition_Oxford,basePath_Oxford,"export_oa.csv")


