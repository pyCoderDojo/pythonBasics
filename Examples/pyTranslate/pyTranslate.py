# https://www.argosopentech.com/argospm/index/

from argostranslate import package, translate

package.install_from_path('models/translate-en_de-1_0.argosmodel')
package.install_from_path('models/translate-de_en-1_0.argosmodel')

installed_languages = translate.get_installed_languages()

print([str(lang) for lang in installed_languages])

translation = installed_languages[0].get_translation(installed_languages[1])
print(translation.translate("scraper"))

translation = installed_languages[1].get_translation(installed_languages[0])
print(translation.translate("Haus"))