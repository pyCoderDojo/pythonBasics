# Passwort Generator

Passwörter begleiten uns den ganzen Tag, aber leider auch sehr schlechte Passwörter.

Ein gutes Passwort besteht möglichst vielen unterschiedlichen Zeichen und hat eine ausreichende Länge. 

Hier eine gute Einführung zum Thema Passwörter:
[Passwörter Einfach Erklärt](https://www.youtube.com/watch?v=jtFc6B5lmIM)

Und hier eine Einführung in das Knacken von Passwörtern:
[Password Cracking](https://www.youtube.com/watch?v=7U-RbOKanYs)

## Aufgabe

Versuche doch eine Funktion zu programmieren, die 2 Parameter unterstützt:
1. gewünschte Länge des Passworts
2. gültige Zeichen für das erzeugte Passwort 

Als Rückgabe soll die Funktion das generierte Passwort liefern. 

### Tipps

1. Zufallszahlen liefert random.randint(minWert, maxWert)
1. ein Alphabet für Passwörter könnte in Form eines String definiert werden
    alpha = 'abcd'

## Erweiterung

1. Teste die Qualität der Passwörter. Erzeuge 500.000 Passwörter und prüfe ob das gleiche Passwort mehrfach erzeugt wird. 

1. Teste mit einer Länge von 2,3,4,5,6,7,8,9 und 10, was beobachtest du?

## Links

1. [Passwörter Einfach Erklärt (Alexander Lehmann)](https://www.youtube.com/watch?v=jtFc6B5lmIM)

