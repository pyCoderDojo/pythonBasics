# Buchstaben Statistik 

Jede Sprache kennzeichnet sich durch unterschiedliche Häufigkeiten mit denen Buchstaben in Texten vorkommen. 

## Aufgabe

Erstelle aus einem Text eine Statisik der vorkommenden Buchstaben. 

Die Aufgabe könnte in zumindest 2 Ausbaustufen umgesetzt werden: 
1. Ausgabe der Vorkommen eines jeden Buchstaben der u.g. Alphabete.
2. Berechnung der prozentualen Häufigkeit eines jeden Buchstaben bezogen auf die Gesamtzeichenanzahl (beschränkt auf die Zeichen aus den u.g. Alphabeten)

# Tipps 

Erstelle die Statistik auf Basis dieser Alphabete:

* Texte in deutscher Sprache: 'abcdefghijklmnopqrstuvwxyzäüöß'
* Texte in englischer Sprache: 'abcdefghijklmnopqrstuvwxyz'

Im Ordner daten findest du Texte:
* kant-01.txt (DE)
* kant-02.txt (DE)
* morison-01.txt (EN)
* wells-01.txt (EN)

Für die Umsetzung können folgende Basics besonders nützlich sein:

1. 02_Listen.py
1. 02_Schleifen.py
1. 02_Woerterbuecher.py
1. 08_Dateien.py
