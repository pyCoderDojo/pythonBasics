# Passphrase Generator

Passwörter begleiten uns den ganzen Tag, aber leider auch sehr schlechte Passwörter.

Ein gutes Passwort kann auch eine Passphrase sein. Also eine Reihe von Worten, die wir uns besser merken können. Im besten Fall ist eine Passphrase auch ein Satz, denn wir uns um so besser merken können. 

## Aufgabe

Versuche doch eine Funktion zu programmieren, die 2 Parameter unterstützt:
1. gewünschte Länge des Passworts
2. gültige Zeichen für das erzeugte Passwort 

Als Rückgabe soll die Funktion das generierte Passwort liefern. 

### Tipps

1. Zufallszahlen liefert random.randint(minWert, maxWert)
1. ein Alphabet für Passwörter könnte in Form eines String definiert werden
    alpha = 'abcd'

## Erweiterung

1. Teste die Qualität der Passwörter. Erzeuge 500.000 Passwörter und prüfe ob das gleiche Passwort mehrfach erzeugt wird. 

1. Teste mit einer Länge von 1,2 und 3, was beobachtest du?   
