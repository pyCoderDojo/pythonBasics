# https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life

from icecream import ic
import random
import time

ic.enable()

def debug(spielfeld):
    for y in range(1, spielfeld.y + 1):
        for x in range(1, spielfeld.x + 1):
            nb = spielfeld.getNachbarn(x, y)
            off = (x + (y - 1) * spielfeld.y) - 1
            ic(x, y, off, nb)
            nbV = spielfeld.getNachbarnVals(x, y)
            ic(nbV)

    spielfeld.print()


class Spielfeld:

    def __init__(self, x=10, y=10, filename=None, test=False, rnd=False):
        self.display = {
            0: ' ',
            1: '*'
        }
        if filename == None:
            self.x = x
            self.y = y
            self.sp = []
            self.sp = self.__initSp(x, y, test, rnd)
            self.test = test
        else:
            ic(filename)
            x = 0
            y = 0
            for l in open(filename, 'r'):
                l = l.strip()
                if l > "":
                    if x == 0:
                        x = len(l)
                    elif x != len(l):
                        print("invalid data file")
                        exit(1)
                    y += 1
            self.x = x
            self.y = y
            self.sp = self.__initSp(x, y, False, False)
            off = 0
            for l in open(filename, 'r'):
                l = l.strip()
                if l > "":
                    for c in l:
                        if c == '0':
                            self.sp[off] = 0
                        else:
                            self.sp[off] = 1
                        off += 1
            self.test = False

    def print(self):
        for i in range(len(self.sp)):
            if self.test:
                print(f"%03d " % self.sp[i], end='')
            else:
                # print(f"%01d" % self.sp[i], end='')
                print(f"%s" % self.display[self.sp[i]], end='')

            # check for linebreak
            if (i + 1) % self.x == 0:
                print("")

    def __initSp(self, x, y, test=False, rnd=False):
        sp = []
        for i in range(x * y):
            if test:
                sp.append(i + 1)
            else:
                sp.append(random.randint(0, 1) if rnd else 0)
        return sp

    def __offset(self, x, y):
        return (((y - 1) * self.x) + x) - 1

    def getNachbarn(self, x, y):

        def add(n):
            if n > 0:
                nachbarn.append(n)

        nachbarn = []
        curCell = self.__offset(x, y)

        # row above left
        add(curCell - (self.x - 1) if y > 1 and x < self.x else 0)
        # row above center
        add(curCell - (self.x) if y > 1 else 0)
        # row above right
        add(curCell - (self.x + 1) if y > 1 and x > 1 else 0)

        # left side
        add(curCell - 1 if x > 1 else 0)
        # right side
        add(curCell + 1 if x < self.x else 0)

        # row below left
        add(curCell + (self.x - 1) if y < self.y and x > 1 else 0)
        # row below center
        add(curCell + (self.x) if y < self.y else 0)
        # row below right
        add(curCell + (self.x + 1) if y < self.y and x < self.x else 0)

        return nachbarn

    def getNachbarnVals(self, x, y):
        nb = self.getNachbarn(x, y)
        nbVals = []
        for i in nb:
            nbVals.append(self.sp[i - 1])

        return nbVals

    def getNachbarnCount(self, x, y):
        count = 0
        nbV = self.getNachbarnVals(x, y)
        for n in nbV:
            if n > 0:
                count += 1
        return count

    def nextGen(self):
        spNg = self.__initSp(self.x, self.y, test=False, rnd=False)

        for y in range(1, self.y + 1):
            for x in range(1, self.x + 1):
                off = (self.__offset(x, y)) - 1
                nbC = spielfeld.getNachbarnCount(x, y)
                if self.sp[off] > 0 and nbC in [2, 3]:
                    spNg[off] = 1
                elif self.sp[off] == 0 and nbC in [3]:
                    spNg[off] = 1
                else:
                    spNg[off] = 0

        self.sp = spNg


## start with random cells
# maxx = 20
# maxy = 10
# spielfeld = Spielfeld(maxx, maxy, test=False, rnd=True)

## start with cells loaded from a file
spielfeld = Spielfeld(filename='daten/gof004.txt')

while True:
    spielfeld.print()
    spielfeld.nextGen()
    print("-" * spielfeld.x)
    time.sleep(1)
