import random

kleinbuchstaben = "abcdefghijklmnopqrstuvwxyz"
grossbuchstaben = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
zahlen = "1234567890"
sonderzeichen = "!@#$%^&*()_-=+"
passwortAlphabet = kleinbuchstaben + grossbuchstaben + zahlen + sonderzeichen


def generierePasswort(laenge, alphabet):
    passwort = []
    for i in range(1, laenge + 1):
        zufall = random.randint(0, len(alphabet) - 1)
        p = alphabet[zufall]
        passwort.append(p)

    return "".join(passwort)


def testIt(pwdLen):
    pwdCount = 500000
    pwds = set()
    for i in range(1, pwdCount + 1):
        pwds.add(generierePasswort(pwdLen, passwortAlphabet))
        # pwds.add("aaaa")

    if len(pwds) == pwdCount:
        print(f"passwords with length %d, all %d passwords are unique!" % (pwdLen, pwdCount))
    else:
        print(f"passwords with length %d, %d/%d passwords are not unique (%5.2f %s) !" % (pwdLen, len(pwds), pwdCount, 100-((100/pwdCount)*len(pwds)), "%"))

print(f"ein Passwort: %s" % generierePasswort(16, passwortAlphabet))

for i in range(2,11):
    testIt(i)
