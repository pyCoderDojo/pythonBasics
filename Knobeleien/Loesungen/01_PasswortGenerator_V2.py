import random


def genGen(pwdLen, pwdAlpha):
    passwort = []
    for i in range(0, pwdLen):
        randIndex = random.randint(0, len(pwdAlpha) - 1)
        char = pwdAlpha[randIndex]
        passwort.append(char)
    return "".join(passwort)


alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789%$#@*()"

stats = {}

tests = 50000
for z in range(0, 5):
    for j in range(2, 10 + 1):
        # print(f"current password len %d" % j)
        passwords = set()
        for i in range(0, tests):
            passwords.add(genGen(j, alphabet))
        lenPasswords = len(passwords)

        dubs = tests - lenPasswords
        if not str(j) in stats.keys():
            stats[str(j)] = [dubs]
        else:
            statList = stats[str(j)]
            statList.append(dubs)
            stats[str(j)] = statList

for u in stats.keys():
    stat = stats[u]
    avg = sum(stat) / len(stat)
    print(f"%s : %5.2f" % (u, avg))
