
alphabetDE = 'abcdefghijklmnopqrstuvwxyzäüöß'
alphabetEN = 'abcdefghijklmnopqrstuvwxyz'

def getStats(filename, alpha):
    stats = {}
    count = 0

    for line in open(filename, 'r'):
        for c in line:
            lC = c.lower()
            if lC in alpha:
                if lC in stats.keys():
                    stat = stats[lC]
                    stat +=1
                    stats[lC] = stat
                else:
                    stats[lC] = 1
                count += 1

    keys = list(stats.keys())
    keys.sort()

    sstats = {}
    for k in keys:
        val = stats[k]
        p = (100 / count) * val
        key = f"%5.2f" % p
        sstats[key] = k
    return sstats

def process(filename, alpha):
    sstats = getStats(filename, alpha)
    skeys = list(sstats.keys())
    skeys.sort()
    skeys.reverse()
    print(filename)
    for k in skeys:
        v = sstats[k]
        print(f"%s : %s" % (v, k))

process('../daten/kant-01.txt', alphabetDE)
process('../daten/kant-02.txt', alphabetDE)
process('../daten/morison-01.txt', alphabetEN)
process('../daten/wells-01.txt', alphabetEN)
process('../daten/saint-exupery-01.txt', alphabetEN)