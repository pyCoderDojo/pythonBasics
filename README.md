# pythonBasics

Diese Repo enthält einfache Beispiel die Grundlagen der Programmiersprache Python verdeutlichen. 

Die Beispiele sollten mit wenig Vorkenntnissen verständlich sein und schnell einen groben Überblick vermitteln. 

Für den tieferen Einstieg empfehle ich die Website [Python lernen](https://www.python-lernen.de)

## Autor

* derMicha
* dermicha (ät) cyber4edu (pünktchen) org
* https://coderdojo.red
* https://cyber4edu.org
